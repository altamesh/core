package com.logitive.lws.core.search;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.logitive.lws.core.ValidationException;
import com.logitive.lws.core.domain.search.SearchConfig;
import com.logitive.lws.core.service.search.ISearchService;

public class ConfigTest {
	private final static Logger LOGGER = Logger.getLogger(ConfigTest.class.getName());
	private static EJBContainer ejbContainer;
	private static Context ctx;

	@AfterClass
	public static void end() {
		ejbContainer.close();
	}

	@BeforeClass
	public static void initEjb() throws Exception {

		Map<String, File> properties = new HashMap<String, File>();
		properties.put(EJBContainer.MODULES, new File("target/classes"));
		ejbContainer = EJBContainer.createEJBContainer(properties);
		ctx = ejbContainer.getContext();
	}

	//@Test 
	public void readWritePromotion() throws javax.validation.ValidationException,
			NamingException, ValidationException {
		ISearchService searchService = (ISearchService) ctx
				.lookup("java:global/classes/SearchCoreService");
		SearchConfig uIConfig = searchService.readUIConfig();
		assert uIConfig == null : "uIConfig should be null";
		uIConfig= new SearchConfig();
		searchService.writeUIConfig(uIConfig);
		uIConfig = searchService.readUIConfig();
		assert uIConfig != null : "uIConfig should not be null";
		LOGGER.info(uIConfig.getId());
		searchService.removeUIConfig(uIConfig);
	}
}
