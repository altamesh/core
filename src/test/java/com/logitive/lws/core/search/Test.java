package com.logitive.lws.core.search;

import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.request.CoreAdminRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CoreAdminParams.CoreAdminAction;

public class Test {

	public static void main(String args[]) throws IOException,
		SolrServerException {
		boost();
		//reload();
	}

	public void query() throws MalformedURLException, SolrServerException {
		HttpSolrServer solr = new HttpSolrServer("http://localhost:8983/solr");

		SolrQuery query = new SolrQuery();
		query.setQuery("sony digital camera");
		query.addFilterQuery("cat:electronics", "store:amazon.com");
		query.setFields("id", "price", "merchant", "cat", "store");
		query.setStart(0);
		query.set("defType", "edismax");

		QueryResponse response = solr.query(query);
		SolrDocumentList results = response.getResults();
		for (int i = 0; i < results.size(); ++i) {
			System.out.println(results.get(i));
		}
	}

	public void add() throws SolrServerException, IOException {
		HttpSolrServer server = new HttpSolrServer("http://localhost:8983/solr");
		for (int i = 0; i < 1000; ++i) {
			SolrInputDocument doc = new SolrInputDocument();
			
			doc.addField("cat", "book");
			doc.addField("id", "book-" + i);
			doc.addField("name", "The Legend of the Hobbit part " + i);
			server.add(doc);
			if (i % 100 == 0)
				server.commit(); // periodically flush
		}
		server.commit();
	}

	public void push() {

	}

	public void revert() {

	}

	public static void boost() throws SolrServerException, IOException {
		HttpSolrServer server = new HttpSolrServer("http://localhost:8983/solr");
		SolrInputDocument doc = new SolrInputDocument();
		doc.setDocumentBoost(0.5f);
		doc.addField("cat", "book");
		doc.addField("id", "book-7");
		doc.addField("name", "The Legend of the Hobbit part ", 1.5f);
		server.add(doc);
		server.commit();

		doc = new SolrInputDocument();
		doc.setDocumentBoost(0.6f);
		doc.addField("cat", "book");
		doc.addField("id", "book-8");
		doc.addField("name", "The Legend of the Hobbit part ", 1.3f);
		server.add(doc);
		server.commit();
	}

	public static void reload() throws SolrServerException, IOException {
		CoreAdminRequest adminRequest = new CoreAdminRequest();
		adminRequest.setAction(CoreAdminAction.RELOAD);

		//CoreAdminResponse adminResponse = adminRequest
			//	.process(new HttpSolrServer("http://localhost:8983/solr"));
		//NamedList<NamedList<Object>> namedList = adminResponse.getCoreStatus();

	}
}
