package com.logitive.lws.core.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import com.logitive.lws.core.ValidationError;
import com.logitive.lws.core.ValidationException;
import com.logitive.lws.core.Validator;
import com.logitive.lws.core.domain.DomainObject;


@Stateless
// @TransactionAttribute(TransactionAttributeType.MANDATORY)
public class GenericCoreService implements GenericService {
	private static final Logger LOGGER = Logger
			.getLogger(GenericCoreService.class.getName());
	@PersistenceContext(unitName = "ffbesm", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public GenericCoreService() {
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <T extends DomainObject> List<T> findAll(Class<T> claz) {
		String qstr = "SELECT p FROM " + claz.getSimpleName() + " p  ";
		Query q = entityManager.createQuery(qstr);
		List<T> list = (List<T>) q.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <T extends DomainObject> List<T> findAll(Class<T> claz, int start,
			int size) {
		String qstr = "SELECT p FROM " + claz.getSimpleName() + " p  ";
		Query q = entityManager.createQuery(qstr);
		q.setFirstResult(start);
		q.setMaxResults(size);
		List<T> list = (List<T>) q.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <T extends DomainObject> List<T> findAllWhere(Class<T> claz,
			String attribute, String value) {
		String qstr = "SELECT p FROM " + claz.getSimpleName() + " p  WHERE p."
				+ attribute + " =  '" + value + "'";
		Query q = entityManager.createQuery(qstr);
		List<T> list = (List<T>) q.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <T extends DomainObject> T findById(Class<T> claz, String id) {
		String qstr = "SELECT p FROM " + claz.getSimpleName()
				+ " p WHERE p.id =  '" + id + "'";
		Query q = entityManager.createQuery(qstr);
		Object object = null;
		try {
			object = q.getSingleResult();
		} catch (NoResultException e) {
			LOGGER.info(e.getMessage() + "  no result for " + q);
		}
		return (T) object;
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <T extends DomainObject> T findByAttribute(Class<T> claz,
			String attribute, String value) {
		String qstr = "SELECT p FROM " + claz.getSimpleName() + " p WHERE p."
				+ attribute + " =  '" + value + "'";
		Query q = entityManager.createQuery(qstr);
		Object object = null;
		try {
			object = q.getSingleResult();
		} catch (NoResultException e) {
			LOGGER.info(e.getMessage() + "  no result for " + q);
		}
		return (T) object;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public <T extends DomainObject> List<T> findListByAttribute(Class<T> claz,
			String attribute, String value) {
		String qstr = "SELECT p FROM " + claz.getSimpleName() + " p WHERE p."
				+ attribute + " =  '" + value + "'";
		
		Query q = entityManager.createQuery(qstr);
		List<T> list = (List<T>) q.getResultList();
		return list;
	}

	
	

	public DomainObject[] save(DomainObject... objects)
			throws ValidationException {
		List<DomainObject> domainObjectList = new ArrayList<DomainObject>();
		LOGGER.log(Level.INFO, "saving " + objects);
		for (DomainObject object : objects) {
			validate(object);
		}
		for (DomainObject object : objects) {
			object.setLastModified(new Date());
			domainObjectList.add(entityManager.merge(object));
		}
		entityManager.flush();
		return (DomainObject[]) domainObjectList.toArray();
	}

	public Set<ValidationError> validate(DomainObject object)
			throws ValidationException {
		Validator<Object> validat = new Validator<Object>();
		LOGGER.log(Level.INFO, "validating " + object);
		Set<ValidationError> vset = new HashSet<ValidationError>();
		vset = validat.validate(object);
		LOGGER.log(Level.INFO, "validation error size " + vset.size());
		if (!vset.isEmpty()) {
			throw new ValidationException(vset);
		}
		return vset;
	}

	public <T extends DomainObject> List<T> find(Class<T> claz, String query) {
		String qstr = "SELECT p FROM " + claz.getSimpleName() + " p '" + query
				+ "'";
		Query q = entityManager.createQuery(qstr);
		@SuppressWarnings("unchecked")
		List<T> object = q.getResultList();
		return object;
	}

	public void remove(DomainObject object) {
		object = entityManager.find(object.getClass(),
				((DomainObject) object).getId());// really need it ?
		entityManager.remove(object);
		entityManager.flush();
		
	}

	public <T extends DomainObject> T save(T object) throws ValidationException {
		LOGGER.log(Level.INFO, "saving " + object);
		validate(object);
		object.setLastModified(new Date());
		object = entityManager.merge(object);
		entityManager.flush();
		return object;
	}
}
