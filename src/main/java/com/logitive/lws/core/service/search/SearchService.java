package com.logitive.lws.core.service.search;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import com.logitive.lws.core.service.GenericCoreService;
import com.logitive.lws.core.ValidationException;
import com.logitive.lws.core.domain.search.SearchConfig;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SearchService extends GenericCoreService implements
		ISearchService {

	//private final static Logger LOGGER = Logger
		//	.getLogger(SearchCoreService.class.getName());

	public SearchConfig readUIConfig() {
		List<SearchConfig> list = findAll(SearchConfig.class);
		if (!list.isEmpty())
			return list.get(0);
		return null;
	}

	public void writeUIConfig(SearchConfig uIConfig) throws ValidationException {
		save(uIConfig);		
	}
	
	public void removeUIConfig(SearchConfig uIConfig){
		remove(uIConfig);
	}

}
